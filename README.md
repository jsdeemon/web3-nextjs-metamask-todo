## Ethereum decentralized app based on NexJs + Truffle + Ganache

1. Download Ganache and run 
https://trufflesuite.com/ganache/ 

2. Install truffle globally: 

```bash
$ npm install -g truffle
```
3. Install node modules in root folder 
```bash
$ npm install
```

Go to the backend folder and run 

```bash
$ truffle migrate
```
or
```bash
$ truffle migrate --reset
```
Copy the contract address from console and put it into
client/config.js

Go to the client folder and run
```bash
$ npm install
```

Run the nextJs app:
```bash
$ npm run dev
```